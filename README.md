# Virtual Card

## 虚拟信用卡简介

虚拟信用卡是一种只存在于网络上的信用卡，不具备实体信用卡的物理形态。

它主要依靠数字化技术和互联网来实现消费支付、账单查询、账户管理等功能。虚拟信用卡通常由银行或其他金融机构发行，用户可以在网上申请并通过证件核实后使用。

## Onekey Card

[Onekey Card](https://blockwander.com/onekey-card/)是可以使用USDC充值的Visa虚拟卡，可以绑定微信和支付宝进行消费，也可以充值ChatGPT和OpenAI，绑定支付渠道后：

- 线下支付：需要商家使用的 pos 机支持 Visa 或 Master 通道
- 线上支付：支付成功与否受账户信誉度及消费频率影响

目前注册 Onekey Card 需要填写邀请码：ZFEBXZ
